<?php
class Food extends Database{
	
	protected $food_id = NULL;
	protected $food_name = NULL;
	protected $food_store_name = NULL;
	protected $food_price = NULL;
	protected $food_image = NULL;
	protected $food_description = NULL;
	
	function __construct(){
		$this->connect();	
	}
	
	public function set_food_id($food_id){
		$this->food_id = $food_id;
	}
	public function get_food_id(){
		return $this->food_id;
	}
	//

	public function set_food_name($food_name){
		$this->food_name = $food_name;
	}
	public function get_food_name(){
		return $this->food_name;
	}
	//
	public function set_food_store_name($food_store_name){
		$this->food_store_name = $food_store_name;
	}
	public function get_food_store_name(){
		return $this->food_store_name;
	}
	//
	public function set_food_price($food_price){
		$this->food_price = $food_price;
	}
	public function get_food_price(){
		return $this->food_price;
	}
	//
	public function set_food_image($food_image){
		$this->food_image = $food_image;
	}
	public function get_food_image(){
		return $this->food_image;
	}
	//
	public function set_food_description($food_description){
		$this->food_description = $food_description;
	}
	public function get_food_description(){
		return $this->food_description;
	}
	//
	public function add(){
		
		$sql = "SELECT * FROM foods
				WHERE food_name = '$this->food_name'";
		$this->query($sql);
		if($this->num_rows() > 0){
			return 'food exist';
		}
		else{
			$sql = "INSERT INTO foods( food_name, food_store_name, food_price)
					VALUES('$this->food_name', 
						   '$this->food_store_name',  
						   '$this->food_price');";
			$this->query($sql);	
		}
	}
	public function edit(){
		
		$sql = "SELECT * FROM foods
				WHERE food_name = '$this->food_name'
				AND food_id != $this->food_id
				";
		$sql;
		$this->query($sql);
		if($this->num_rows() > 0){
			return  'food exist';
		}
		else{
			$sql = "UPDATE foods SET food_name = '$this->food_name',
									 food_store_name = '$this->food_store_name',
									 food_price = $this->food_price
					WHERE food_id = $this->food_id";
			$this->query($sql);
		}
		
	}
	public function del(){
		$sql = "DELETE FROM foods
				WHERE food_id = $this->food_id";
		$this->query($sql);
	}
}

?>