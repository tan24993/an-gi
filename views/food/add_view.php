<div class="container">
    <div id="navbar" class="row">
    	<div class="col-sm-12">
        	<nav class="navbar navbar-default">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php?controller=user&act=listed">Home</a></li>
                        <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Users
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?controller=user&act=listed">List</a></li>
                            <li><a href="index.php?controller=user&act=add">Add user</a></li>
                        </ul>
                        </li>
                        <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Foods
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?controller=food&act=listed">List</a></li>
                            <li><a href="index.php?controller=food&act=add">Add food</a></li>
                        </ul>
                        </li> 
                    </ul>
                    <p id="logout" class="navbar-text navbar-right"><a class="navbar-link" href="index.php?controller=user&act=logout">Logout</a></p>
                  </div>
            </nav>
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-6">
        	<?php
            if(isset($_SESSION['error'])){
				echo $_SESSION['error'];
				unset($_SESSION['error']);
			}
			?>
        	<form method="post">
                <div class="form-group">
                	<label>Name</label>
                    <input type="text" name="food" class="form-control" placeholder="Food" required />
                </div>
                <div class="form-group">
                    <label>Store Name</label>
                    <input type="text" name="store" class="form-control" placeholder="Store" required />
                </div>
                <div class="form-group">
                	<label>Price</label>
                    <input type="text" name="price" class="form-control" placeholder="Price" required />
                </div>
                <div class="form-group">
                    <label>Images</label>
                    <input type="file" name="image">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="3" name="description"></textarea>
                </div>
                <input type="submit" name="submit" value="Thêm mới" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>