<div class="container">
    <div id="navbar" class="row">
    	<div class="col-sm-12">
        	<nav class="navbar navbar-default">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li><a href="index.php?controller=user&act=listed">Home</a></li>
                        <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Users
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?controller=user&act=listed">List</a></li>
                            <li><a href="index.php?controller=user&act=add">Add user</a></li>
                        </ul>
                        </li>
                        <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Foods
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?controller=food&act=listed">List</a></li>
                            <li><a href="index.php?controller=food&act=add">Add food</a></li>
                        </ul>
                        </li> 
                    </ul>
                    <p id="logout" class="navbar-text navbar-right"><a class="navbar-link" href="index.php?controller=user&act=logout">Logout</a></p>
                </div>
            </nav>
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-6">
        	
            <?php
            if(isset($_SESSION['error'])){
				echo $_SESSION['error'];
				unset($_SESSION['error']);
			}
			?>
        	<form method="post">
            	
                <div class="form-group">
                	<label>Username</label>
                    <input type="text" name="user" class="form-control" placeholder="Username" value="<?php echo $row['user_name'];?>" required />
                </div>
                <div class="form-group">
                	<label>Password</label>
                    <input type="password" name="pass" class="form-control" placeholder="Password" value="<?php echo $row['user_pass'];?>" required />
                </div>
                
                <div class="form-group">
                	<label>Level</label>
                    <select name="level" class="form-control">
                    	<option <?php if($row['user_level'] == 1){echo 'selected';}?> value="1">Admin</option>
                        <option <?php if($row['user_level'] == 2){echo 'selected';}?> value="2">User</option>
                    </select>
                </div>
                <input type="submit" name="submit" value="Sửa" class="btn btn-primary" />
            </form>
        </div>
    </div>
</div>

