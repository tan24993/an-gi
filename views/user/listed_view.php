<div class="container">
    <div id="navbar" class="row">
    	<div class="col-sm-12">
        	
            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <ul class="nav navbar-nav">
                    <li><a href="index.php?controller=user&act=listed">Home</a></li>
                    <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Users
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="index.php?controller=user&act=listed">List</a></li>
                        <li><a href="index.php?controller=user&act=add">Add user</a></li>
                    </ul>
                    </li>
                    <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Foods
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="index.php?controller=food&act=listed">List</a></li>
                        <li><a href="index.php?controller=food&act=add">Add food</a></li>
                    </ul>
                    </li> 
                </ul>
                <p id="logout" class="navbar-text navbar-right"><a class="navbar-link" href="index.php?controller=user&act=logout">Logout</a></p>
              </div>
            </nav>
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-12">
        	<?php
            if(isset($_SESSION['report'])){
				echo $_SESSION['report'];
				unset($_SESSION['report']);
			}
			?>
        	<table class="table table-striped">
            	<tr id="tbl-first-row">
                	<td width="10%">Id</td>
                    <td width="50%">Username</td>
                    <td width="20%">Level</td>
                    <td width="10%">Edit</td>
                    <td width="10%">Delete</td>
                </tr>
                <?php
                while($row = $User->fetch_row()){
				?>
                <tr>
                	<td><?php echo $i++;?></td>
                    <td><?php echo $row['user_name'];?></td>
                    <td><?php echo $row['user_level'];?></td>
                    <td><a href="index.php?controller=user&act=edit&user_id=<?php echo $row['user_id'];?>">Edit</a></td>
                    <td><a href="index.php?controller=user&act=del&user_id=<?php echo $row['user_id'];?>">Delete</a></td>
                </tr>
                <?php
				}
				?>
			</table>
            <div aria-label="Page navigation">
            	<ul class="pagination">
                	<?php
                    echo $Pagination->get_list_pages();
					?>
                </ul>
            </div>
        </div>
    </div>
</div>

