<?php
if($_SESSION['level'] != 1){
	header('location:index.php?controller=user&act=login');
}

if(isset($_POST['submit'])){
	
	$User = new User();
	$User->set_user_name($_POST['user']);
	$User->set_user_pass($_POST['pass']);
	$User->set_user_level($_POST['level']);
	
	if($User->add() == 'user exist'){
		$_SESSION['error'] = '<div class="alert alert-danger">User exist!</div>';
	}
	else{
		$_SESSION['report'] = '<div class="alert alert-success">Add user success!</div>';
		header('location: index.php?controller=user&act=listed');
	}

}

include_once('views/user/add_view.php');
?>