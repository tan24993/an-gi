<?php
if(isset($_SESSION['level'])){
	if($_SESSION['level'] == 1){
		header('location:index.php?controller=user&act=listed');
	}	
}
if(isset($_POST['submit'])){
	
	$User = new User();
	$User->set_user_name($_POST['user']);
	$User->set_user_pass($_POST['pass']);
	
	if($User->login() == 'login fail'){
		$_SESSION['error'] = '<div class="alert alert-danger">Account not valid!</div>';
	}
	else{
		$_SESSION['level'] = 1;
		header('location:index.php?controller=user&act=listed');
	}
}

include_once('views/user/login_view.php');
?>