<?php
if($_SESSION['level'] != 1){
	header('location:index.php?controller=user&act=login');
}

if(isset($_POST['submit'])){
	
	$Food = new food();
	$Food->set_food_name($_POST['food']);
	$Food->set_food_store_name($_POST['store']);
	$Food->set_food_price($_POST['price']);
	$Food->set_food_image($_POST['image']);
	$Food->set_food_description($_POST['description']);
	
	if($Food->add() == 'food exist'){
		$_SESSION['error'] = '<div class="alert alert-danger">Food exist!</div>';
	}
	else{
		$_SESSION['report'] = '<div class="alert alert-success">Add food success!</div>';
		header('location: index.php?controller=food&act=listed');
	}

}

include_once('views/food/add_view.php');
?>