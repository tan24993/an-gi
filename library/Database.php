<?php
class Database{
	protected $db_host = 'localhost';
	protected $db_user = 'root';
	protected $db_pass = '';
	protected $db_name = 'mon_an';
	
	protected $row = NULL;
	protected $rows = NULL;
	protected $conn = NULL;
	protected $result = NULL;
	
	public function connect(){
		
		$this->conn = mysqli_connect($this->db_host, $this->db_user, $this->db_pass, $this->db_name);
		if($this->conn){
			mysqli_query($this->conn, "SET NAMES 'utf8'");
		}
		else{
			echo 'Ket noi that bai!';
		}
	}
	
	public function free_result(){
		
		if($this->result){
			mysqli_free_result($this->result);
		}	
	}
	
	public function query($sql){
		
		$this->free_result();
		$this->result = mysqli_query($this->conn, $sql);
	}
	
	public function num_rows(){

		if($this->result){
			$this->rows = mysqli_num_rows($this->result);
		}
		return $this->rows;
	}
	
	public function fetch_row(){
		
		if($this->result){
			$this->row = mysqli_fetch_array($this->result);
			
		}
		return $this->row;
	}	
}




?>