<?php
session_start();
include_once('library/__autoload.php');
include_once('library/Database.php');
include_once('library/Pagination.php');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Users manager</title>
<link rel="stylesheet" href="library/bootstrap-3.3.7-dist/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<style>
#login{
	float:none;
	margin:50px auto;}
#navbar{
	margin-top:50px;}
#tbl-first-row{
	font-weight:bold;}
#logout{
	padding-right:30px;}
#navbar{
	margin-top:50px;}
#tbl-first-row{
	font-weight:bold;}
#logout{
	padding-right:30px;}
#navbar{
	margin-top:50px;}
#tbl-first-row{
	font-weight:bold;}	
#logout{
	padding-right:30px;}			
</style>
</head>
<body>

<?php
if(isset($_GET['controller'])){
	switch($_GET['controller']){
		case 'user': include_once('controllers/user/user.php');
		break;
		case 'food': include_once('controllers/food/food.php');
		break;
	}
}
else{
	header('location:index.php?controller=user&act=login');
}
?>

</body>
</html>